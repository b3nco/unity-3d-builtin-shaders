Unity 3D Builtin Shaders (Unofficial)
=====================

A history (v2.0.0 onwards) of all the buildin shader releases from unity as a handy git.

This is an unofficial repo maintained as a useful resource for developers to view the code and changes across releases.

Shaders are publically available from:
http://unity3d.com/unity/download/archive

Every release downloaded as a replacement and added with a commit and tag as follows:

git add -A

git commit -m 'Builtin Shaders X.X.X'

git tag -a 'vX.X.X' -m 'builtin_shaders-X.X.X.zip'

A tag is added even if no changes exist.